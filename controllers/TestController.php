<?php
/*
 *  2) Настроить в web.php компонент test с ключом class равным \app\components\TestService::class,
 *  выполнить в TestController-е метод компонента test, передать результат выполнения во вьюху и там вывести.
 */

namespace app\controllers;

use app\models\Product;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\db\Query;


class TestController extends Controller
{

    public function actionIndex()
    {
        
        return \Yii::$app->test->get();


        $test = new TestService();
        $test->attr = 'value';
        return $test->get();
        $product = new Product();
        
        $product->id = 1;
        $product->name = 'test product';
        $product->description = 'description';
        $product->category_id = '1';
        $product->price = '$100.00';
        
        return $this->render('index', ['product' => $product]);
    }
/*
 * 3) В экшене insert TestController-а через
 * Yii::$app->db->createCommand()->insert() вставить несколько записей в таблицу user,
 * $connection->createCommand()->insert('user', [
 */
    public function actionInsert() {

        Yii::$app->db->createCommand()->insert('user', [
            'username' => 'test username',
            'name' => 'test name',
            'password_hash' => 'testhash'
        ])->execute();

    }
/*
 * 4) Используя \yii\db\Query в экшене select TestController выбрать из user:
а) Запись с id=1
б) Все записи с id>1 отсортированные по имени (orderBy())
в) количество записей.
Выборку можно вывести просто через VarDumper или функцию _end() если подключили funcs.php.
 */

    /*
     * 6) Используя \yii\db\Query в экшене select TestController-а вывести содержимое note
с присоединенными по полю creator_id записями из таблицы user (innerJoin())
     */
    public function actionSelect() {

        $dbQuery1 = new Query();
        $dbQuery2 = new Query();
        $dbQuery3 = new Query();
        $dbQuery4 = new Query();

        $query1 = $dbQuery1->select('*')->from('user')->where(['id' => '10'])->all();
        $query2 = $dbQuery2->select('*')->from('user')->where(['>', 'id', '4'])->limit(6)->orderBy(['name' => SORT_DESC])->all();
        $query3 = $dbQuery3->select('*')->from('user')->count();

        $query4 = $dbQuery4->select('*')->from(['n' => 'note'])->innerJoin(['u' => 'user'], 'n.creator_id=u.id')->all();

        _dump($query1);
        _dump($query2);
        _dump($query3);
        _dump($query4);

    }

    /*
     * 5) В экшене insert TestController-а через Yii::$app->db->createCommand()->batchInsert() вставить одним вызовом сразу 3 записи в таблицу note,
в поле creator_id подставив реальное значение id из user.
     */

    public function actionInsertbatch() {

        Yii::$app->db->createCommand()->batchInsert('note', ['text', 'creator_id', 'created_at'], [
            ['text test 1', 20, mktime()],
            ['text test 2', 30, mktime()],
            ['text test 3', 40, mktime()]
        ])->execute();

        $dbQuery = new Query();

        $query = $dbQuery->select('*')->from('note')->all();
        _dump($query);

    }

}
