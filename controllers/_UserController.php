<?php
/*
 * В UserController создать экшен test, в нем используя ActiveRecord, т.е. методы классов User и Note,
выполнить последовательно, наблюдая выполняемые запросы в debug-панели:
а) Создать запись в таблице user.
б) Создать три связаные (с записью в user) запиcи в note, используя метод link().
в) Прочитать из базы все записи из User применив жадную подгрузку связанных данных из Note, с запросами без JOIN.
г) Прочитать из базы все записи из User применив жадную подгрузку связанных данных из Note, с запросом содержащем JOIN.
 */

namespace app\controllers;

use app\models\Access;
use app\models\Note;
use app\models\User;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;


class UserController extends Controller
{

	public function actionTest()
	{

		/*Создать запись в таблице user

		/*$user = new User();
		$user->name = 'User3';
		$user->username = 'User3';
		$user->password_hash = 'hash3';
		_dump($user->save());
		*/

		/*б) Создать три связаные (с записью в user) запиcи в note, используя метод link().

		$user = User::findOne(['name' => 'User1']);
		$note = new Note();
		$note->text = 'New note2 text';
		$note->created_at = mktime();
		$note->link('creator',$user);

		_dump($user->notes);
		*/

		//в) Прочитать из базы все записи из User применив жадную подгрузку связанных данных из Note, с запросами без JOIN.*/

//		$user = User::find()->with(User::RELATION_NOTES)->where(['name' => 'User1'])->one();

		//г) Прочитать из базы все записи из User применив жадную подгрузку связанных данных из Note, с запросом содержащем JOIN.

//		$user = User::find()->joinwith(User::RELATION_NOTES)->where(['name' => 'User1'])->one();


//		6) Добавить с помощью созданного релейшена связь между записями в User и Note (метод link(), обе модели д.б. сохранены).


		$user = User::findOne(['name' => 'User1']);
		$note = Note::findOne(28);


		$user->link(User::RELATION_NOTES_ACCESS, $note);
		
		$user = User::find()->with(User::RELATION_NOTES, User::RELATION_ACCESS)->where(['name' => 'User1'])->one();


		return $this->renderContent(
			VarDumper::dumpAsString($user, 4, true)
		);


	}

}
