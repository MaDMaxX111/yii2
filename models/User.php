<?php

namespace app\models;

use Yii;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $name
 * @property string $surname
 * @property string $password_hash
 * @property string $access_token
 * @property string $auth_key
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Access[] $accesses
 * @property Access[] $accessedNotes
 * @property Note[] $notes
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

    /*
     * Создать в классе User свойство password и добавить
     * его в одно из правил rules(), убрать password_hash из
     * required правила
     */

    public $password;

    const RELATION_NOTES        = 'notes';
    const RELATION_ACCESS       = 'accesses';
    const RELATION_NOTES_ACCESS = 'accessedNotes';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /*
	 * Подключить в классах User и Note поведение TimestampBehavior -
	 * проверить как оно работает при создании и изменении записей в базе.
	 */
    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'updatedAtAttribute' => FALSE
            ]
        ];
    }

    /**
     * @inheritdoc
     */

    public function rules() {
        return [
            [['username', 'name', 'password'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['username', 'name', 'surname', 'password', 'access_token', 'auth_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'            => 'ID',
            'username'      => 'Username',
            'name'          => 'Name',
            'surname'       => 'Surname',
            'password'      => 'Password',
            'password_hash' => 'Password Hash',
            'access_token'  => 'Access Token',
            'auth_key'      => 'Auth Key',
            'created_at'    => 'Created At',
            'updated_at'    => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccesses() {
        return $this->hasMany(Access::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes() {
        return $this->hasMany(Note::className(), ['creator_id' => 'id']);
    }

    /*
	 * Добавить в User метод релейшена getAccessedNotes, связывающий User и Note через релейшен accesses,
	см. http://stuff.cebe.cc/yii2docs-ru/guide-db-active-record.html#junction-table.
	 */
    public function getAccessedNotes() {
        return $this->hasMany(Note::className(), ['id' => 'note_id'])
            ->via(self::RELATION_ACCESS);
    }

    /*
     * Реализовать там же в методе beforeSave хэширование непустого пароля в аттрибут password_hash
и заполнение случайной строкой аттрибута auth_key при создании записи.
     */
    public function beforeSave($insert) {

        if (!parent::beforeSave($insert)) {
            return FALSE;
        }

        if ($this->password) {
            $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }
        if ($this->isNewRecord) {
            $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
        }

        return TRUE;
    }

    /*
     * Имплементировать в User интерфейс IdentityInterface и реализовать необходимые методы
как показано здесь http://stuff.cebe.cc/yii2docs-ru/guide-security-authentication.html#implementing-identity.
     */
    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     *
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     *
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = NULL) {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /*
     *  Реализовать в User методы validatePassword($password) и findByUsername($name).
Первый проверит пароль сравнив его хэш с текушим значением из атрибута password_hash, проверка описана по последней ссылке.
Второй вернет модель пользователя по значению username.
     */

    public static function findByUsername($username)
    {

        return self::findOne([
                'name' => $username]
        );
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    public static function find() {
        return new \app\models\UserQuery(get_called_class());
    }
}
