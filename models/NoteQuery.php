<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Note]].
 *
 * @see Note
 */
class NoteQuery extends \yii\db\ActiveQuery
{
//    public function active()
//    {
//        return $this->andWhere('[[status]]=1');
//    }

    /**
     * @inheritdoc
     * @return Note[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Note|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /*
     * а) В NoteQuery сделать метод byCreator($userId), который должен добавлять условие по id создателя (creator_id = $userId). Для этого можно изменить имеющийся там закоментированный метод active().
     *
     */

    public function byCreator($userId)
    {
        return $this->where(['creator_id' => $userId]);
    }
}
