<?php
/*
 * CREATE TABLE `note` (
`id` INT NOT NULL AUTO_INCREMENT,
`text` TEXT NOT NULL,
`creator_id` INT NOT NULL,
`created_at` INT NULL,
PRIMARY KEY (`id`)
);
 */
use yii\db\Migration;

/**
 * Class m180125_062647_test_2
 */
class m180125_062647_test_2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('note', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'creator_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->null()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180125_062647_test_2 cannot be reverted.\n";
        $this->dropTable('note');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180125_062647_test_2 cannot be reverted.\n";

        return false;
    }
    */
}
