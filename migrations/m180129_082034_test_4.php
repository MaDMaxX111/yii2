<?php

use yii\db\Migration;

/**
 * Class m180129_082034_test_4
 */
class m180129_082034_test_4 extends Migration
{
    /**
     * @inheritdoc
     */
    
    public function safeUp()
    {
        \Yii::$app->db->createCommand()-> addForeignKey('fx_access_user', 'access', ['user_id'], 'user', ['id'])->execute();
        \Yii::$app->db->createCommand()-> addForeignKey('fx_access_note', 'access', ['note_id'], 'note', ['id'])->execute();
        \Yii::$app->db->createCommand()-> addForeignKey('fx_note_user', 'note', ['creator_id'], 'user', ['id'])->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180129_082034_test_4 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_082034_test_4 cannot be reverted.\n";

        return false;
    }
    */
}
