<?php
/*
 * CREATE TABLE `access` (
`id` INT NOT NULL AUTO_INCREMENT,
`note_id` INT NOT NULL,
`user_id` INT NOT NULL,
PRIMARY KEY (`id`)
)
 */
use yii\db\Migration;

/**
 * Class m180125_062652_test_3
 */
class m180125_062652_test_3 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('access', [
            'id' => $this->primaryKey(),
            'note_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180125_062652_test_3 cannot be reverted.\n";
        $this->dropTable('access');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180125_062652_test_3 cannot be reverted.\n";

        return false;
    }
    */
}
