<?php

/*
 * CREATE TABLE `user` (
`id` INT NOT NULL AUTO_INCREMENT,
`username` VARCHAR(255) NOT NULL,
`name` VARCHAR(255) NOT NULL,
`surname` VARCHAR(255) NULL,
`password_hash` VARCHAR(255) NOT NULL,
`access_token` VARCHAR(255) NULL DEFAULT NULL,
`auth_key` VARCHAR(255) NULL DEFAULT NULL,
`created_at` INT NULL,
`updated_at` INT NULL,
PRIMARY KEY (`id`)
);
 */
use yii\db\Migration;

/**
 * Class m180125_060657_test_1
 */
class m180125_060657_test_1 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'surname' => $this->string(255)->null(),
            'password_hash' => $this->string(255)->notNull(),
            'access_token' => $this->string(255)->null()->defaultValue(NULL),
            'auth_key' => $this->string(255)->null()->defaultValue(NULL),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180125_060657_test_1 cannot be reverted.\n";

        $this->dropTable('user');

        return TRUE;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180125_060657_test_1 cannot be reverted.\n";

        return false;
    }
    */
}
