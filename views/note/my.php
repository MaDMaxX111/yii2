<?php
/*
 * в) Создать вьюху my скопировав и изменив вьюху index - в таблице д.б. только столбцы text created_at actions.
 * Убедиться, что теперь в списке заметок выводятся только созданные вами.
 */
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\NoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="note-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Note', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'text:ntext',
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {share}',
                'buttons' => [
                    'share' => function($url, $model, $key) {
                        return Html::a(\yii\bootstrap\Html::icon('share'), ['access/create', 'noteId' => $model->id]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
