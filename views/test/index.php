<?php
 /*
  * @var $this yii\web\View
  * @var $product \app\models\Product
  */

$this->title = 'My Test Controller';
?>
<div class="site-index">
    <h1><?= $this->title;?></h1>
    <div>
        <?= \yii\widgets\DetailView::widget(['model' => $product]); ?>
    </div>
</div>
